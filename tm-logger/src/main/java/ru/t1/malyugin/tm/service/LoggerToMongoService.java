package ru.t1.malyugin.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import lombok.SneakyThrows;
import org.bson.Document;
import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.api.ILoggerService;

import java.util.LinkedHashMap;
import java.util.Map;

public final class LoggerToMongoService implements ILoggerService {

    @NotNull
    private final ObjectMapper objectMapper = new ObjectMapper();

    @NotNull
    private final MongoClient mongoClient = new MongoClient("localhost", 27017);

    @NotNull
    private final MongoDatabase mongoDatabase = mongoClient.getDatabase("TM_LOG");

    @Override
    @SneakyThrows
    public void log(@NotNull final String text) {
        @NotNull final Map<String, Object> event = objectMapper.readValue(text, LinkedHashMap.class);
        @NotNull final String collectionName = event.get("table").toString();
        @NotNull final MongoCollection<Document> collection = mongoDatabase.getCollection(collectionName);
        collection.insertOne(new Document(event));
    }

}