package ru.t1.malyugin.tm.service.dto;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.api.repository.dto.ITaskDTORepository;
import ru.t1.malyugin.tm.api.service.IServiceLocator;
import ru.t1.malyugin.tm.api.service.dto.ITaskDTOService;
import ru.t1.malyugin.tm.dto.model.TaskDTO;
import ru.t1.malyugin.tm.exception.entity.ProjectNotFoundException;
import ru.t1.malyugin.tm.exception.entity.TaskNotFoundException;
import ru.t1.malyugin.tm.exception.field.*;
import ru.t1.malyugin.tm.repository.dto.TaskDTORepository;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

public final class TaskDTOService extends AbstractWBSDTOService<TaskDTO> implements ITaskDTOService {

    public TaskDTOService(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    protected ITaskDTORepository getRepository(@NotNull final EntityManager entityManager) {
        return new TaskDTORepository(entityManager);
    }

    @Override
    public TaskDTO create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(name)) throw new NameEmptyException();
        @NotNull final TaskDTO taskDTO = new TaskDTO();
        taskDTO.setName(name);
        if (!StringUtils.isBlank(description)) taskDTO.setDescription(description);
        add(userId, taskDTO);
        return taskDTO;
    }

    @Override
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        if (StringUtils.isBlank(name)) throw new NameEmptyException();
        @Nullable final TaskDTO taskDTO = findOneById(userId, id);
        if (taskDTO == null) throw new TaskNotFoundException();
        taskDTO.setName(name);
        if (!StringUtils.isBlank(description)) taskDTO.setDescription(description);
        update(taskDTO);
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(projectId)) return Collections.emptyList();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            return getRepository(entityManager).findAllByProjectId(userId, projectId);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String taskId,
            @Nullable final String projectId
    ) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(projectId)) throw new ProjectIdEmptyException();
        if (StringUtils.isBlank(taskId)) throw new TaskIdEmptyException();
        if (getServiceLocator().getProjectDTOService().findOneById(userId, projectId) == null)
            throw new ProjectNotFoundException();
        @Nullable final TaskDTO taskDTO = findOneById(userId, taskId);
        if (taskDTO == null) throw new TaskNotFoundException();
        taskDTO.setProjectId(projectId);
        update(userId, taskDTO);
    }

    @Override
    public void unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String taskId,
            @Nullable final String projectId
    ) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(projectId)) throw new ProjectIdEmptyException();
        if (StringUtils.isBlank(taskId)) throw new TaskIdEmptyException();
        if (getServiceLocator().getProjectDTOService().findOneById(userId, projectId) == null)
            throw new ProjectNotFoundException();
        @Nullable final TaskDTO taskDTO = findOneById(userId, taskId);
        if (taskDTO == null) throw new TaskNotFoundException();
        taskDTO.setProjectId(null);
        update(userId, taskDTO);
    }

}